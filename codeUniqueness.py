import pandas as pdd
import dask.dataframe as dd
from dask.distributed import Client


#uniqueness.json contain frequency count of every header divided by the total count of all the headers
udf = pdd.read_json('gs://pathToFile/uniqueness.json') 
def test_fun(headernames):
    uniqueness = 0
    headernames['bool'] = headernames['header_name'].isin(udf.index)
    #headerNamesList.columns=['ip','match','bool']
    for index, row in headernames.iterrows():
        if not row['bool']:
            #print(0.99)
            uniqueness += 0.99
    
    
    udf['bool2'] = udf.index.isin(headernames['header_name'])
    for index2, row2 in udf.iterrows():
        if row2['bool2']:
            uniqueness +=udf['uniqueness'][index2]
        else:
            uniqueness +=1.0 - udf['uniqueness'][index2]
    return uniqueness
    

for i in range(12,27):
    print(i)
    client = Client('145.100.104.126:8786')
    df = dd.read_parquet("gs://pathToFile/part."+str(i)+".parquet",engine='fastparquet')
    df = df.repartition(npartitions=4*60)
    df = df.set_index('ip')
    df1 = df['headers'].str.extractall('(\n[\w\-]*\:|^[\w\-]*:)')
    df1.columns=['header_name']
    df1['header_name'] = df1['header_name'].str.lstrip()
    df1= df1.reset_index()
    df1=client.persist(df1)
    df2 = df1.groupby('ip').apply(lambda group: test_fun(group) , meta=('int')).compute(schduler='processes')
    df2.to_json('part.0'+str(i)+'-uniquenessAll.json')
    client.restart()