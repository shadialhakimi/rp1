import dask.dataframe as dd
from dask.distributed import Client



def ordering(row):
    test = row.sort_values()
    result = []
    for i in test.index:
        if test[i] > -1:
            result.append(i)
    #test['order'] = result
    return result
    
for i in range(1,27): # this is the number of dataset partition files
    print(i)
    client = Client('145.100.104.126:8786')
    df = dd.read_parquet("gs://pathToFile/part."+str(i)+".parquet",engine='fastparquet')
    df = df.repartition(npartitions=4*60)
    df = df.drop_duplicates(subset=['ip'])
    df = df.set_index('ip')
    df = client.persist(df)
    df2 = df['headers'].str.find('Connection:')
    temp= df['headers'].str.find('Date:')
    df2= df2.to_frame().merge(temp.to_frame(),left_index=True, right_index=True)
    listNames=['Content-Type:','Server:','Content-Length:']
    for name in listNames:
        temp=df['headers'].str.find(name)
        df2= df2.merge(temp.to_frame(),left_index=True, right_index=True)
    df2.columns=[1,2,3,4,5]
    newResult = df2.apply(lambda raw: ordering(raw),axis=1,meta=(None, 'object')).compute(schduler='processes')
    newResult.to_json('orderAll/part.'+str(i)+'-orderAll.json')
    client.restart()