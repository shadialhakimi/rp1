from io import BytesIO
import base64
import pandas as pd
import json
import hashlib
import re
import numpy as np
import multiprocessing as mp
from fastparquet import ParquetFile, write
from os import listdir


def preprocessing(row):
    try:
        #print(row[2])
        #decoding to bytes opbject
        base64_message = row[2]
        base64_bytes = base64_message.encode('utf8')
        message_bytes = base64.b64decode(base64_bytes)
        #trying to split the headers and body in different ways
        splitbytes = re.search(b'(?:\r?\n){2}',message_bytes).group()
        splitbody =re.split(b'(?:\r?\n){2}',message_bytes,1)
        headers = splitbody[0]
        body = splitbody[1]
        #print(message_bytes)
        #print(headers)
        #print(body)
        #trying to split the statusline from the headers in 2 different ways
        try:
            status, header = headers.split(b"\r\n", 1)
            statussep = "\r\n"
        except:
            try:
                status, header = headers.split(b"\n", 1)
                statussep = "\n"
            except:
                #print('no headers found')
                #print(i)
                #print(headers)
                #print(message_bytes)
                header = b''
                statussep = splitbytes.decode("utf-8")[:]
                splitbytes = b''
        #trying to make a string out of the status and headers
        try:
            finalstatus = status.decode("utf-8")
            finalstatus += statussep
            finalheader = header.decode("utf-8")
            finalheader += splitbytes.decode("utf-8")
        except:
            #print('decoding error')
            return {'ip': row[2]}
        #exporting headers to new dataframe
        return {'ip': row[1],'statusline': finalstatus, 'headers': finalheader, 'bodylength': len(body), 'bodyhash': hashlib.sha256(body).hexdigest()}
    except:
        #print('decoding error')
        return {'ip': row[1]}
    #handling decoding error to different dataframe
    #print("decoding error")
    #print(i)
    #errorData100 = errorData100.append({'ip': http100_df['ip'][i],'data': message_bytes}, ignore_index=True)

def main():
    pool = mp.Pool(mp.cpu_count())
    fileNames = listdir('pathToFile/file.parquet/')
    counter = 0
    for file in fileNames:
        if file.endswith('.parquet'):
            http100_df = ParquetFile('pathToFile/file.parquet/'+file)
            http100_df = http100_df.to_pandas()
            result = pool.imap(preprocessing, http100_df.itertuples(name=None), chunksize=10)
            output = [x for x in result]
            counter += 1
            print(counter)
            outputjson = json.dumps(output)
            with open('dataSet.json', 'a') as outfile:
                outfile.write(outputjson)
            if counter > 5:
                break

if __name__ == "__main__":
  main()
